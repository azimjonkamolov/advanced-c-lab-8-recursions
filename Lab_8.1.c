// Name: Advanced C Lab 8 Recursions
// Time: 12:07 11.09.2018
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: factorial with recursion
// This program consists of two functions

#include<stdio.h>       // HEADER FILE

int recit(long int n)   // RECURION FUNCTION TO FIND FACTORIAL OF N
{
	if(n<=0)            // CHECKS WHETHER 0 OR NOT,
	{
		return 1;       // IF SO, THEN RUNS THIS
	}
	else                // IF NOT, RUNS THIS
	{
		return n*recit(n-1); // KEEPS CALLING UNTILL N IS LESS THAN 1
	}
}

int main()
{
	long int n;                 // INT N CARIABLE
	scanf("%li", &n);           // TO ENTER TEH INPUT HERE
	printf("%li", recit(n));    // TO SHOW THE OUTPUT
}