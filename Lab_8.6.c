// Name: Advanced C Lab 8 Recursions
// Time: 13:03 11.09.2018
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to convert a decimal number to binary using recursion
// This program consists of two functions

#include<stdio.h>						// HEADER 

int bn(int n)							// FUNCTION TO TURN DECIMAL NUMBER INTO BINARY
{
	if (n==0)							// IF N IS 0 
		return 0;						// RETURNS 0
	else 								// IF NOT
		return (n%2+10*bn(n/2));		// DO THIS CALCUALTION TO FIND THE BINARY NUMBER 
}

int main()								// THE MAIN FUNCTION
{
	int n;								// INT VARIABLE
	scanf("%d", &n);					// TO ENTER THE INPUT
	printf("%d", bn(n));				// TO GIVE THE ANSWER 
}
