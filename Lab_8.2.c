// Name: Advanced C Lab 8 Recursions
// Time: 12:15 11.09.2018
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: the greatest common denominator GCD using recursion
// This program consists of two functins

#include<stdio.h>       		// HEADER FILE

int ifit(int n, int n1) 		// FUNCTION WITH TWO ARGUMENTS
{
	if (n1 != 0)				// CHECKS IF N1 IS NOT O
       return ifit(n1, n%n1);   // IF SO, THEN DEVIDES N BY N1 AND KEEPS RUNNING
    else 
       return n;				// IF NOT, THEN GIVES N AS AN OUTPUT
}

int main()
{
	int n, n1, i; 				// INT N N1 AND I VARIABLES
	scanf("%d %d", &n, &n1);	// TO ENTER THE INPUT N AND N1
	printf("%d", ifit(n,n1));   // TO SHOW THE OUTPUT	
}
