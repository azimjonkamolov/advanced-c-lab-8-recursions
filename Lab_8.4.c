// Name: Advanced C Lab 8 Recursions
// Time: 12:34 11.09.2018
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: Fibonacci using recursion
// This program consists of two functions

#include<stdio.h>						// HEADER 

int fibit(int n)						// FUNCTION WITH INT N ARGUMENT
{
	if (n==1 && n ==0)					// CHECKS IF N IS 1 AND N IS 0
		return 1;						// THEN RUNS THIS
	else if(n>1)						// IF NOT, THEN RUNS THIS
		return fibit(n-1)+fibit(n-2);	// KEEPS DECREASING AND FINALLY GIVES THE ANSWER AS A RETURN VALUE
}

int main()
{
	int n;								// INT VARIABLE N
	scanf("%d", &n);					// TO ENTER THE INPUT
	printf("%d", fibit(n));				// TO SHOW THE OUTPUT
	
}
