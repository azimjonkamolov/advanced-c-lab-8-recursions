// Name: Advanced C Lab 8 Recursions
// Time: 12:23 11.09.2018
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to rite a program to compute Euler’s number
// This program consists of two functions

#include<stdio.h>       		// HEADER FILE


double fibit(int n, double x)	// DOUBLE FUNCTION WITH INT AND DOUBLE VARIABLE
{
	float f;					// FLOAT VARIABLE
	int i;						// INT VARIABLE
	if (n==0)					// TO CHECK IF N IS 0 OR NOT
		return 0;				// GIVES 0 AS A RETUR VALUE	
	else 						// IF NOT, THEN RUNS THIS
	for(i=n-1;i>0;--i)			// FOR LOOP TO DEREACE N UNTIL I IS GREATER THAN ZERO
		f=1+x*f/i;				// TO CALCULATE 
	return f;					// RETURN THE ANSWER
}

int main()
{
	int n;						// INT VARIABLE N
	float x=1.0;				// FLOAT VARIABLE X WITH 1.0
	scanf("%d", &n);			// TO ENTER N
	printf("%.6f", fibit(n, x));// TO GIVE THE ANSWER WITH 6 DECIMAL POINTS
	
}
