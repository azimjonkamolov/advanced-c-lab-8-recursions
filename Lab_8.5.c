// Name: Advanced C Lab 8 Recursions
// Time: 12:50 11.09.2018
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to check if a number is a prime number or not using recursion
// This program consists of two functions

#include<stdio.h>						// HEADER 

int pnum(int n, int i)					// FUNCTION WITH TWO INT VARIABLES
{
	if (i==1)							// CHECKS IF I IS 1 OR NOT
	{
		return 1;						// IF SO, THEN RETURNS 1
	}
	else								// IF NOT, THEN RUNS THIS
	{
		if(n%i==0)						// CHECKS IF N DEVIDED BY I IS 0 OR NOT
		{
			return 0;					// IF SO RETURNS 0
		}
		else							// IF NOT, RUNS THIS
		{
			return pnum(n, i-1);       // KEEPS DECREASING I AND KEEPS CALLING THE FUNCTION 
		}
	}
	
}

int main()							 // THE MAIN FUNCTION 
{
	int p, ch;						// INT VARIABLES P AND CH
	scanf("%d", &p);				// TO ENTER P
	ch=pnum(p, p/2);				// TO GET THE VALUE OF PNUM FUNC INTO CH
	if(ch == 1)						// CHECKS IF CH IS 1 OR NOT HERE IT IS CHECKING A FINAL VALUE
	{
		printf("1");				// IF SO, THEN 1
	}
	else							// ELSE RUNS THIS
	{
		printf("0");				// GIVES 0
	}
	
}
